# 0.1.0 - initial release
## Major Updates
* adding coro type that has a one-call init
* adding replayable type that can be restored from recorded actions
* adding a set of assertion helpers for verifying the above types

# 0.2.0 - frozen
## Major Updates
* changing Replayable to Recorded
  * no replay option - the guts of what is recorded has changed, and there
  were no non-test uses of the replay
* removed assertion helpers
  * conflated action/validation
  * messy internally
  * tied users to our assertion lib, which is unnecessary and not the point of
    rototiller
* Coro and Recorded return semantic responses to send/throw actions:
  * Yielded (contains the yielded value)
  * Raised (contains the raised exception and traceback)
  * Returned (contains the returned value)
## Minor Updates
* test system updates
* improvements to typing
* passing more static checks

# 0.3.0 - frozen
## Minor Updates
* added a 'close' method
* checking in this CHANGELOG finally.
## Patch Updates
* made Raised.value a proper property
