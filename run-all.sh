#! /usr/bin/env bash

set -e

# it must build
python check-build.py --name rototiller --version 0.3.0
python check-imports.py
vulture setup.py
# it must behave correctly
python run-tests.py test.py
# it must not be obviously insecure
bandit -r .
# it must be consistently defined
mypy --ignore-missing-imports --follow-imports silent .
# it must conform to community standards
flake8 --exclude '*/test*.py,test*.py' --config .flake8
flake8 --filename '*/test*.py,test*.py' --config .flake8.test
pylint *.py
isort -rc . --check-only --diff -lai 2
python find-commented-code.py
