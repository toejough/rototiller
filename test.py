"""Tests for rototiller."""


# [ Imports:Python ]
import sys
import types
import typing

# [ Imports:Third Party ]
import dado
import pipe  # type: ignore

# [ Imports:Project ]
import rototiller


# [ Internals ]
@pipe.Pipe  # type: ignore
def _is_multiline(string: str) -> bool:
    return 1 < len(string.splitlines())


def _indent(string: str) -> str:
    return '  ' + '\n  '.join(string.splitlines())


def _limit_lines(string: str, *, max_lines: int) -> str:
    lines = string.splitlines()
    allowed_lines = lines[:max_lines]
    lines_left = len(lines) - len(allowed_lines)
    if lines_left:
        allowed_lines.append(f"<<<< +{lines_left} more >>>>")
    return '\n'.join(allowed_lines)


def _format_assertion(**message_kwargs: typing.Any) -> str:
    message = f"`Assertion failed!"
    for key, value in message_kwargs.items():
        value_string = f"{value}"
        if value_string | _is_multiline:
            value_string = _limit_lines(value_string, max_lines=20)
            value_string = "\n" + _indent(value_string)
        message += f"\n{key}: {value_string}"
    return message


def _raise_assertion(**message_kwargs: typing.Any) -> None:
    message = _format_assertion(**message_kwargs)
    raise AssertionError(message)


def _assert(success: bool, **message_kwargs: typing.Any) -> None:
    if not success:
        _raise_assertion(**message_kwargs)


def _get_exc_info(
    message: str,
) -> typing.Tuple[typing.Optional[typing.Type[BaseException]], typing.Optional[BaseException], typing.Optional[types.TracebackType]]:
    try:
        raise RuntimeError(message)
    except RuntimeError:
        return sys.exc_info()


def _get_error(message: str) -> RuntimeError:
    return RuntimeError(message)


def _test_gen_func(message: str) -> typing.Generator[str, str, str]:
    while message != 'return':
        if message == 'raise':
            raise RuntimeError('error message')
        try:
            message = yield f"yielding on {message}"
        except GeneratorExit:
            if message == 'raise on close':
                message = yield 'this should cause a runtime error'
            raise
        except Exception as error:  # pylint: disable=broad-except
            message = str(error)
    return 'return message'


@types.coroutine
def _test_types_coro_func(message: str) -> typing.Generator[str, str, str]:
    return (yield from _test_gen_func(message))


async def _test_async_func(message: str) -> str:
    return await _test_types_coro_func(message)


# [ Tests ]
@dado.data_driven(['coro_type', 'gen_func'], {
    'coro_gen_func': [rototiller.Coro, _test_gen_func],
    'coro_types_coro_func': [rototiller.Coro, _test_types_coro_func],
    'coro_async_func': [rototiller.Coro, _test_async_func],
    'recorded_coro_gen_func': [rototiller.RecordedCoro, _test_gen_func],
    'recorded_coro_types_coro_func': [rototiller.RecordedCoro, _test_types_coro_func],
    'recorded_coro_async_func': [rototiller.RecordedCoro, _test_async_func],
})
def test_yields_on_init_with(
    coro_type: rototiller.Coro[str, str, str],
    gen_func: typing.Callable[[str], typing.Union[
        typing.Coroutine[str, str, str],
        typing.Generator[str, str, str],
    ]],
) -> None:
    # mypy complaining this isn't callable...
    coro = coro_type(gen_func)  # type: ignore

    # When
    result = coro.init('init')

    # Then
    _assert(isinstance(result, rototiller.Yielded), message="`coro.init` result was not a `rototiller.Yielded`!", init_result=result)
    result = typing.cast(rototiller.Yielded[str], result)
    _assert(
        result.value == 'yielding on init',
        message="`coro.init` result value was not as expected!",
        init_result_value=result.value,
        expected='yielding on init',
    )


@dado.data_driven(['coro_type', 'gen_func'], {
    'coro_gen_func': [rototiller.Coro, _test_gen_func],
    'coro_types_coro_func': [rototiller.Coro, _test_types_coro_func],
    'coro_async_func': [rototiller.Coro, _test_async_func],
    'recorded_coro_gen_func': [rototiller.RecordedCoro, _test_gen_func],
    'recorded_coro_types_coro_func': [rototiller.RecordedCoro, _test_types_coro_func],
    'recorded_coro_async_func': [rototiller.RecordedCoro, _test_async_func],
})
def test_yields_on_send_with(
    coro_type: rototiller.Coro[str, str, str],
    gen_func: typing.Callable[[str], typing.Union[
        typing.Coroutine[str, str, str],
        typing.Generator[str, str, str],
    ]],
) -> None:
    # Given
    # mypy complaining this isn't callable...
    coro = coro_type(gen_func)  # type: ignore

    # When
    coro.init('yield')
    result = coro.send('send')

    # Then
    _assert(isinstance(result, rototiller.Yielded), message="`coro.send` result was not a `rototiller.Yielded`!", send_result=result)
    result = typing.cast(rototiller.Yielded[str], result)
    _assert(
        result.value == 'yielding on send',
        message="`coro.send` result value was not as expected!",
        send_result_value=result.value,
        expected='yielding on send',
    )


@dado.data_driven(['coro_type', 'gen_func', 'error_func'], {
    'error_with_coro_gen_func': [rototiller.Coro, _test_gen_func, _get_error],
    'error_with_coro_types_coro_func': [rototiller.Coro, _test_types_coro_func, _get_error],
    'error_with_coro_async_func': [rototiller.Coro, _test_async_func, _get_error],
    'error_with_recorded_coro_gen_func': [rototiller.RecordedCoro, _test_gen_func, _get_error],
    'error_with_recorded_coro_types_coro_func': [rototiller.RecordedCoro, _test_types_coro_func, _get_error],
    'error_with_recorded_coro_async_func': [rototiller.RecordedCoro, _test_async_func, _get_error],
    'exc_info_with_coro_gen_func': [rototiller.Coro, _test_gen_func, _get_exc_info],
    'exc_info_with_coro_types_coro_func': [rototiller.Coro, _test_types_coro_func, _get_exc_info],
    'exc_info_with_coro_async_func': [rototiller.Coro, _test_async_func, _get_exc_info],
    'exc_info_with_recorded_coro_gen_func': [rototiller.RecordedCoro, _test_gen_func, _get_exc_info],
    'exc_info_with_recorded_coro_types_coro_func': [rototiller.RecordedCoro, _test_types_coro_func, _get_exc_info],
    'exc_info_with_recorded_coro_async_func': [rototiller.RecordedCoro, _test_async_func, _get_exc_info],
})
def test_yields_on_throw(
    coro_type: rototiller.Coro[str, str, str],
    gen_func: typing.Callable[[str], typing.Union[
        typing.Coroutine[str, str, str],
        typing.Generator[str, str, str],
    ]],
    error_func: typing.Callable[[str], typing.Union[
        typing.Tuple[BaseException],
        typing.Tuple[
            typing.Optional[typing.Type[BaseException]],
            typing.Optional[BaseException],
            typing.Optional[types.TracebackType],
        ],
    ]],
) -> None:
    # Given
    # mypy complaining this isn't callable...
    coro = coro_type(gen_func)  # type: ignore

    # When
    coro.init('init')
    result = coro.throw(error_func('error'))

    # Then
    _assert(
        isinstance(result, rototiller.Yielded),
        message="`coro.throw` result was not a `rototiller.Yielded`!",
        throw_result=result,
    )
    result = typing.cast(rototiller.Yielded[str], result)
    _assert(
        result.value == 'yielding on error',
        message="`coro.throw` result value was not as expected!",
        throw_result_value=result.value, expected='yielding on error',
    )


@dado.data_driven(['coro_type', 'gen_func'], {
    'coro_gen_func': [rototiller.Coro, _test_gen_func],
    'coro_types_coro_func': [rototiller.Coro, _test_types_coro_func],
    'coro_async_func': [rototiller.Coro, _test_async_func],
    'recorded_coro_gen_func': [rototiller.RecordedCoro, _test_gen_func],
    'recorded_coro_types_coro_func': [rototiller.RecordedCoro, _test_types_coro_func],
    'recorded_coro_async_func': [rototiller.RecordedCoro, _test_async_func],
})
def test_raises_on_init_with(
    coro_type: rototiller.Coro[str, str, str],
    gen_func: typing.Callable[[str], typing.Union[
        typing.Coroutine[str, str, str],
        typing.Generator[str, str, str],
    ]],
) -> None:
    # Given
    # mypy complaining this isn't callable...
    coro = coro_type(gen_func)  # type: ignore

    # When
    result = coro.init('raise')

    # Then
    _assert(isinstance(result, rototiller.Raised), message="`coro.init` result was not an `rototiller.Raised`!", init_result=result)
    result = typing.cast(rototiller.Raised, result)
    _assert(
        result.exc_type == RuntimeError,
        message="`coro.init` result error type was not as expected!",
        init_result_type=result.exc_type,
        expected=RuntimeError,
    )
    _assert(
        str(result.exc_value) == 'error message',
        message="`coro.init` result error string was not as expected!",
        init_result_error_string=result.exc_value,
        expected='error message',
    )


@dado.data_driven(['coro_type', 'gen_func'], {
    'coro_gen_func': [rototiller.Coro, _test_gen_func],
    'coro_types_coro_func': [rototiller.Coro, _test_types_coro_func],
    'coro_async_func': [rototiller.Coro, _test_async_func],
    'recorded_coro_gen_func': [rototiller.RecordedCoro, _test_gen_func],
    'recorded_coro_types_coro_func': [rototiller.RecordedCoro, _test_types_coro_func],
    'recorded_coro_async_func': [rototiller.RecordedCoro, _test_async_func],
})
def test_raises_on_send_with(
    coro_type: rototiller.Coro[str, str, str],
    gen_func: typing.Callable[[str], typing.Union[
        typing.Coroutine[str, str, str],
        typing.Generator[str, str, str],
    ]],
) -> None:
    # Given
    # mypy complaining this isn't callable...
    coro = coro_type(gen_func)  # type: ignore

    # When
    coro.init('yield')
    result = coro.send('raise')

    # Then
    _assert(isinstance(result, rototiller.Raised), message="`coro.send` result was not an `rototiller.Raised`!", send_result=result)
    result = typing.cast(rototiller.Raised, result)
    _assert(
        result.exc_type == RuntimeError,
        message="`coro.send` result error type was not as expected!",
        send_result_type=result.exc_type,
        expected=RuntimeError,
    )
    _assert(
        str(result.exc_value) == 'error message',
        message="`coro.send` result error string was not as expected!",
        send_result_error_string=result.exc_value,
        expected='error message',
    )


@dado.data_driven(['coro_type', 'gen_func'], {
    'coro_gen_func': [rototiller.Coro, _test_gen_func],
    'coro_types_coro_func': [rototiller.Coro, _test_types_coro_func],
    'coro_async_func': [rototiller.Coro, _test_async_func],
    'recorded_coro_gen_func': [rototiller.RecordedCoro, _test_gen_func],
    'recorded_coro_types_coro_func': [rototiller.RecordedCoro, _test_types_coro_func],
    'recorded_coro_async_func': [rototiller.RecordedCoro, _test_async_func],
})
def test_raises_on_close_with(
    coro_type: rototiller.Coro[str, str, str],
    gen_func: typing.Callable[[str], typing.Union[
        typing.Coroutine[str, str, str],
        typing.Generator[str, str, str],
    ]],
) -> None:
    # Given
    # mypy complaining this isn't callable...
    coro = coro_type(gen_func)  # type: ignore

    # When
    coro.init('raise on close')
    result = coro.close()

    # Then
    _assert(isinstance(result, rototiller.Raised), message="`coro.close` result was not an `rototiller.Raised`!", close_result=result)
    result = typing.cast(rototiller.Raised, result)
    _assert(
        result.exc_type == RuntimeError,
        message="`coro.close` result error type was not as expected!",
        close_result_type=result.exc_type,
        expected=RuntimeError,
    )
    _assert(
        str(result.exc_value) == 'generator ignored GeneratorExit',
        message="`coro.close` result error string was not as expected!",
        close_result_error_string=result.exc_value,
        expected='error message',
    )


@dado.data_driven(['coro_type', 'gen_func', 'error_func'], {
    'error_with_coro_gen_func': [rototiller.Coro, _test_gen_func, _get_error],
    'error_with_coro_types_coro_func': [rototiller.Coro, _test_types_coro_func, _get_error],
    'error_with_coro_async_func': [rototiller.Coro, _test_async_func, _get_error],
    'error_with_recorded_coro_gen_func': [rototiller.RecordedCoro, _test_gen_func, _get_error],
    'error_with_recorded_coro_types_coro_func': [rototiller.RecordedCoro, _test_types_coro_func, _get_error],
    'error_with_recorded_coro_async_func': [rototiller.RecordedCoro, _test_async_func, _get_error],
    'exc_info_with_coro_gen_func': [rototiller.Coro, _test_gen_func, _get_exc_info],
    'exc_info_with_coro_types_coro_func': [rototiller.Coro, _test_types_coro_func, _get_exc_info],
    'exc_info_with_coro_async_func': [rototiller.Coro, _test_async_func, _get_exc_info],
    'exc_info_with_recorded_coro_gen_func': [rototiller.RecordedCoro, _test_gen_func, _get_exc_info],
    'exc_info_with_recorded_coro_types_coro_func': [rototiller.RecordedCoro, _test_types_coro_func, _get_exc_info],
    'exc_info_with_recorded_coro_async_func': [rototiller.RecordedCoro, _test_async_func, _get_exc_info],
})
def test_raises_on_throw(
    coro_type: rototiller.Coro[str, str, str],
    gen_func: typing.Callable[[str], typing.Union[
        typing.Coroutine[str, str, str],
        typing.Generator[str, str, str],
    ]],
    error_func: typing.Callable[[str], typing.Union[
        typing.Tuple[BaseException],
        typing.Tuple[
            typing.Optional[typing.Type[BaseException]],
            typing.Optional[BaseException],
            typing.Optional[types.TracebackType],
        ],
    ]],
) -> None:
    # Given
    # mypy complaining this isn't callable...
    coro = coro_type(gen_func)  # type: ignore

    # When
    coro.init('yield')
    result = coro.throw(error_func('raise'))

    # Then
    _assert(isinstance(result, rototiller.Raised), message="`coro.throw` result was not an `rototiller.Raised`!", throw_result=result)
    result = typing.cast(rototiller.Raised, result)
    _assert(
        result.exc_type == RuntimeError,
        message="`coro.throw` result error type was not as expected!",
        throw_result_type=result.exc_type,
        expected=RuntimeError,
    )
    _assert(
        str(result.exc_value) == 'error message',
        message="`coro.throw` result error string was not as expected!",
        throw_result_error_string=result.exc_value,
        expected='error message',
    )


@dado.data_driven(['coro_type', 'gen_func'], {
    'coro_gen_func': [rototiller.Coro, _test_gen_func],
    'coro_types_coro_func': [rototiller.Coro, _test_types_coro_func],
    'coro_async_func': [rototiller.Coro, _test_async_func],
    'recorded_coro_gen_func': [rototiller.RecordedCoro, _test_gen_func],
    'recorded_coro_types_coro_func': [rototiller.RecordedCoro, _test_types_coro_func],
    'recorded_coro_async_func': [rototiller.RecordedCoro, _test_async_func],
})
def test_returns_on_init_with(
    coro_type: rototiller.Coro[str, str, str],
    gen_func: typing.Callable[[str], typing.Union[
        typing.Coroutine[str, str, str],
        typing.Generator[str, str, str],
    ]],
) -> None:
    # mypy complaining this isn't callable...
    coro = coro_type(gen_func)  # type: ignore

    # When
    result = coro.init('return')

    # Then
    _assert(isinstance(result, rototiller.Returned), message="`coro.init` result was not a `rototiller.Returned`!", init_result=result)
    result = typing.cast(rototiller.Returned[str], result)
    _assert(
        result.value == 'return message',
        message="`coro.return` result value was not as expected!",
        init_result_value=result.value,
        expected='return message',
    )


@dado.data_driven(['coro_type', 'gen_func'], {
    'coro_gen_func': [rototiller.Coro, _test_gen_func],
    'coro_types_coro_func': [rototiller.Coro, _test_types_coro_func],
    'coro_async_func': [rototiller.Coro, _test_async_func],
    'recorded_coro_gen_func': [rototiller.RecordedCoro, _test_gen_func],
    'recorded_coro_types_coro_func': [rototiller.RecordedCoro, _test_types_coro_func],
    'recorded_coro_async_func': [rototiller.RecordedCoro, _test_async_func],
})
def test_returns_on_send_with(
    coro_type: rototiller.Coro[str, str, str],
    gen_func: typing.Callable[[str], typing.Union[
        typing.Coroutine[str, str, str],
        typing.Generator[str, str, str],
    ]],
) -> None:
    # Given
    # mypy complaining this isn't callable...
    coro = coro_type(gen_func)  # type: ignore

    # When
    coro.init('yield')
    result = coro.send('return')

    # Then
    _assert(isinstance(result, rototiller.Returned), message="`coro.send` result was not a `rototiller.Returned`!", send_result=result)
    result = typing.cast(rototiller.Returned[str], result)
    _assert(
        result.value == 'return message',
        message="`coro.send` result value was not as expected!",
        send_result_value=result.value,
        expected='return message',
    )


@dado.data_driven(['coro_type', 'gen_func'], {
    'coro_gen_func': [rototiller.Coro, _test_gen_func],
    'coro_types_coro_func': [rototiller.Coro, _test_types_coro_func],
    'coro_async_func': [rototiller.Coro, _test_async_func],
    'recorded_coro_gen_func': [rototiller.RecordedCoro, _test_gen_func],
    'recorded_coro_types_coro_func': [rototiller.RecordedCoro, _test_types_coro_func],
    'recorded_coro_async_func': [rototiller.RecordedCoro, _test_async_func],
})
def test_returns_on_close_with(
    coro_type: rototiller.Coro[str, str, str],
    gen_func: typing.Callable[[str], typing.Union[
        typing.Coroutine[str, str, str],
        typing.Generator[str, str, str],
    ]],
) -> None:
    # Given
    # mypy complaining this isn't callable...
    coro = coro_type(gen_func)  # type: ignore

    # When
    coro.init('yield')
    result = coro.close()

    # Then
    _assert(isinstance(result, rototiller.Returned), message="`coro.close` result was not a `rototiller.Returned`!", close_result=result)
    result = typing.cast(rototiller.Returned[str], result)
    _assert(
        result.value is None,
        message="`coro.close` result value was not as expected!",
        close_result_value=result.value,
        expected=None,
    )


@dado.data_driven(['coro_type', 'gen_func', 'error_func'], {
    'error_with_coro_gen_func': [rototiller.Coro, _test_gen_func, _get_error],
    'error_with_coro_types_coro_func': [rototiller.Coro, _test_types_coro_func, _get_error],
    'error_with_coro_async_func': [rototiller.Coro, _test_async_func, _get_error],
    'error_with_recorded_coro_gen_func': [rototiller.RecordedCoro, _test_gen_func, _get_error],
    'error_with_recorded_coro_types_coro_func': [rototiller.RecordedCoro, _test_types_coro_func, _get_error],
    'error_with_recorded_coro_async_func': [rototiller.RecordedCoro, _test_async_func, _get_error],
    'exc_info_with_coro_gen_func': [rototiller.Coro, _test_gen_func, _get_exc_info],
    'exc_info_with_coro_types_coro_func': [rototiller.Coro, _test_types_coro_func, _get_exc_info],
    'exc_info_with_coro_async_func': [rototiller.Coro, _test_async_func, _get_exc_info],
    'exc_info_with_recorded_coro_gen_func': [rototiller.RecordedCoro, _test_gen_func, _get_exc_info],
    'exc_info_with_recorded_coro_types_coro_func': [rototiller.RecordedCoro, _test_types_coro_func, _get_exc_info],
    'exc_info_with_recorded_coro_async_func': [rototiller.RecordedCoro, _test_async_func, _get_exc_info],
})
def test_returns_on_throw(
    coro_type: rototiller.Coro[str, str, str],
    gen_func: typing.Callable[[str], typing.Union[
        typing.Coroutine[str, str, str],
        typing.Generator[str, str, str],
    ]],
    error_func: typing.Callable[[str], typing.Union[
        typing.Tuple[BaseException],
        typing.Tuple[
            typing.Optional[typing.Type[BaseException]],
            typing.Optional[BaseException],
            typing.Optional[types.TracebackType],
        ],
    ]],
) -> None:
    # Given
    # mypy complaining this isn't callable...
    coro = coro_type(gen_func)  # type: ignore

    # When
    coro.init('init')
    result = coro.throw(error_func('return'))

    # Then
    _assert(
        isinstance(result, rototiller.Returned),
        message="`coro.throw` result was not a `rototiller.Returned`!",
        throw_result=result,
    )
    result = typing.cast(rototiller.Returned[str], result)
    _assert(
        result.value == 'return message',
        message="`coro.throw` result value was not as expected!",
        throw_result_value=result.value, expected='return message',
    )


def test_raise_value() -> None:
    # Given
    raised: typing.Optional[rototiller.Raised] = None
    exc_tuple: typing.Optional[rototiller.ThrowableType] = None

    # When
    try:
        raise RuntimeError('on purpose')
    except RuntimeError:
        exc_tuple = sys.exc_info()
        raised = rototiller.Raised(*exc_tuple)

    # Then
    _assert(
        isinstance(raised, rototiller.Raised),
        message="No exception was raised!",
    )
    _assert(
        exc_tuple == raised.value,
        message="sys.exc_info output and raised.value output differ!",
        exc_info=exc_tuple,
        raised=raised,
    )
